import menu from './menu.js'

describe('check if menu renders correctly', () => {
  const menuElem = menu()

  test('number of children should be 3', () => {
    expect(menuElem.childNodes.length).toBe(3)
  })

  test('classlist should contain "menu"', () => {
    expect(menuElem.classList.contains('menu')).toBe(true)
  })

  test('first childs classlist should contain "link"', () => {
    expect(menuElem.childNodes[0].classList.contains('link')).toBe(true)
  })

  test('second childs href should contain "/about"', () => {
    expect(menuElem.childNodes[1].href).toContain('/about')
  })

  test('third childs text should be "Contact"', () => {
    expect(menuElem.childNodes[2].textContent).toBe('Contact')
  })
})
